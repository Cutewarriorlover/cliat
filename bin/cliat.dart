import 'package:cliat/cliat.dart' as cliat;

import 'package:args/args.dart';

void main(List<String> arguments) {
  final parser = ArgParser();
  parser.addCommand('server');
  parser.addCommand('chat');
  var argResults = parser.parse(arguments);
  if (argResults.command!.name == 'server') {
    cliat.startServer();
  } else if (argResults.command!.name == 'chat') {
    cliat.chat();
  }
}
