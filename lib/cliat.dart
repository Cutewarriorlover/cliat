import 'chat.dart';

/// Main starting point to start a cliat server.
/// This makes a `socket.io` server which can the be connected
/// to by the [chat()] function.
void startServer() {
  print('server should start sometime now.');
}

void chat() {
  var chat = Chat();
  chat.display();
}
