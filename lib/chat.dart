import 'dart:io';

import 'package:console/console.dart';
import 'package:console/curses.dart';

/// Main entry point for the chat window. This class
/// should be used like this:
///
/// ```dart
/// var chat = Chat();
/// chat.display();
/// ```
///
/// The variable [messages] represents a list of messages
/// that have been sent.
class Chat extends Window {
  List<String> messages = ['test', 'test2'];

  Chat() : super('Hello');

  @override
  void draw() {
    super.draw();

    for (var i = 0; i < messages.length; i++) {
      var message = messages[i];
      Console.moveCursor(row: i * 5 + 5, column: 5);
      Console.write(message);
    }

    messages.add('test');
  }

  @override
  void initialize() {
    Keyboard.bindKeys(['q', 'Q']).listen((_) {
      close();
      Console.resetAll();
      Console.eraseDisplay();
      exit(0);
    });
  }
}
