A command line chat made in [Dart](https://dart.dev), intended as a learning resource only, not as a replacement for any actual chats like Discord.

## Issues
Please report any issues [here](https://gitea.com/Cutewarriorlover/cliat/issues).
